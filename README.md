Dies soll eine Sammlung von Battlemaps, Modulen und anderen Dingen werden, die für die Verwendung mit Space1889 in Foundry VTT erstellt wurden.

Fragen, Anregungen oder wenn du eigene Sachen beisteuern möchtest:<br>
ProjectAvalanche@larp-lieder.de


**ClockworkMagazin**<br>
Alle 21 unveränderten Ausgaben des Uhrwerk Magazins 2014-2019 mit Beiträgen zu Space1889.


**Coins:**<br>
Ein Modul mit den Bildern echter Münzen aus der Zeit (2x Englishe Pfund, 1x Französiche Franc, 1x deutsche Mark) zur Erweiterung des „Dice So Nice“ Moduls.<br>
Installiertes „Dice So Nice“ Modul absolut nötig.


**Tokens:**<br>
Ein paar Tokens die ich für die „Von Neumann“ des Abenteuers „der Ätherkalkulator“ aus dem Buch „Unter Hochdruck“ erstellt habe.
<br>Drauf- und Frontalsicht.


**VonNeumann:**<br>
Battlemap / Deckpläne des Ätherzeppelin „Von Neumann“ des Abenteuers „der Ätherkalkulator“ aus dem Buch „Unter Hochdruck“.


**Werbung im Sinne der Sache:**<br>
Der Künstler Greg Brunni hat Tokens für Space 1889 erstellt und bietet sie auf seiner eigenen Webseite zum Kauf an:<br>
Marsianer: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-8-steampunk-martians
<br>Soldaten: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-4-steampunk-soldiers
<br>Airmen: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-7-steampunk-airmen
<br>Aliens: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-11-steampunk-aliens


-----------------------------------------------------------------


This is intended to be a collection of battlemaps, modules and other stuff made for use with Space1889 in Foundry VTT.

Questions, suggestions or if you want to contribute your own things:<br>
ProjectAvalanche@larp-lieder.de


**ClockworkMagazin**<br>
All 21 unchanged issues of the German Clockwork Magazine 2014-2019 with contributions to Space1889.


**Coins:**<br>
A module with images of real coins from the time (2x English Pounds, 1x French Francs, 1x German Mark) to expand the "Dice So Nice" module.<br>
Installed "Dice So Nice" module absolutely necessary.


**Tokens:**<br>
A few tokens I created for the "Von Neumann" of the adventure "The Ether Calculator" from the book "Unter Hochdruck".<br>
Top and front view.


**VonNeumann:**<br>
Battlemap / deck plans of the ether zeppelin "Von Neumann" of the adventure "The Aether Calculator" from the book " Unter Hochdruck".


**Advertising in the spirit of the matter:**<br>
Artist Greg Brunni has created tokens for Space 1889 and is offering them for purchase on his own website:
<br>Marsianer: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-8-steampunk-martians
<br>Soldiers: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-4-steampunk-soldiers
<br>Airmen: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-7-steampunk-airmen
<br>Aliens: https://www.gregbrunicreations.com/product-page/sci-fi-tokens-set-11-steampunk-aliens
